import 'package:flutter/material.dart';
class ResultPage extends StatelessWidget{
  var answer1,answer2,answer3;
  ResultPage(this.answer1,this.answer2,this.answer3);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("Results"),
          backgroundColor: Colors.amber,
        ),
        body:
        Center(
            child: Container(
              padding: EdgeInsets.all(0),
              child: ResultBox(answer1,answer2,answer3),
            )));
  }
}
class ResultBox extends StatelessWidget {
  var a1,a2,a3;
  ResultBox(this.a1,this.a2,this.a3);
  @override
  Widget build(BuildContext context) {
  return Text("first answer is $a1 and second answer is $a2 and third answer is $a3");
  }
}